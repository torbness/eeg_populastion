#!/usr/bin/env python

import sys
import os
from os.path import join
import numpy as np
import matplotlib
# matplotlib.use("AGG")
import pylab as plt
import neuron
import LFPy
from neuron import h
from plotting_convention import simplify_axes, mark_subplots


root_folder = '..'
# layer_thickness = 100
random_seed = 123
random_seed_shift = {"L5": 1,
                     "L23": 0,
                     "hbp_L5_TTPC2": 2,
                     "hbp_L23_PC_cADpyr229_1": 3,
                     "hbp_L23_PC_cADpyr229_5": 4,
                     'hbp_L5_TTPC2_cADpyr232_1': 5,
                     "BS1173.CNG.swc": 6,
                     "Cell-6-8-06-S3A.CNG.swc": 7,
                     "2007-07-14-A.CNG.swc": 8,
                     "human_L23": 9,
                     "human_L5": 10,

                     }

convert_to_mouse_list = ["2007-07-14-A.CNG.swc"]

layer_dict = {
    "L23": "L23",
    "human_L23": "human_L23",
    "human_L5": "human_L5",
    'hbp_L5_TTPC2_cADpyr232_1': "L5",
    "2007-07-14-A.CNG.swc": "L23",
    "BS1173.CNG.swc": "L5",
    "L5": "L5",
}
# layer_thickness_dict = {"L1": [0, -100],
#                         "L23": [-100, -320],
#                         "L4": [-320, -450],
#                         "L5": [-450, -700],
#                         "L6": [-700, -950],
layer_densities = {
                    "L23": 20000 * 1e-9,#  cells / um^3
                    "L5":  20000 * 1e-9,
                    "human_L23": 20000 * 1e-9,#  cells / um^3
                    "human_L5": 20000 * 1e-9,#  cells / um^3
                   }
layer_thickness = 200

pulse_centers = {"L23": 10,
                 "L5": 10}

cell_weights = {"2007-07-14-A.CNG.swc": 0.005/60.,
               "BS1173.CNG.swc": 0.005/10.,
               "L23": 0.005,
               "human_L23": 0.005,
               "human_L5": 0.005,
               "L5": 0.005,}


def initialize_population(num_cells, celltype):
    print "Initializing cell positions and rotations ..."
    cell_density = layer_densities[layer_dict[celltype]]
    pop_radius = np.sqrt(num_cells / (cell_density * np.pi * 100))
    print num_cells, pop_radius

    x_y_z_rot = np.zeros((num_cells, 4))

    for cell_number in range(num_cells):
        plt.seed((random_seed + random_seed_shift[celltype]) * cell_number)
        rotation = 2*np.pi*np.random.random()

        z = 0.#layer_thickness * (np.random.random() - 0.5)
        x, y = 2 * (np.random.random(2) - 0.5) * pop_radius
        while np.sqrt(x**2 + y**2) > pop_radius:
            x, y = 2 * (np.random.random(2) - 0.5) * pop_radius
        x_y_z_rot[cell_number, :] = [x, y, z, rotation]

    r = np.array([np.sqrt(d[0]**2 + d[1]**2) for d in x_y_z_rot])
    argsort = np.argsort(r)
    x_y_z_rot = x_y_z_rot[argsort]

    np.save(join(root_folder, 'x_y_z_rot_%d_%s.npy' % (num_cells, celltype)), x_y_z_rot)
    plt.close("all")
    plt.subplot(121, xlabel='x', ylabel='y', aspect=1, frameon=False)
    plt.scatter(x_y_z_rot[:, 0], x_y_z_rot[:, 1], edgecolor='none', s=2)

    plt.subplot(122, xlabel='x', ylabel='z', aspect=1, frameon=False)
    plt.scatter(x_y_z_rot[:, 0], x_y_z_rot[:, 2], edgecolor='none', s=2)

    plt.savefig(join(root_folder, 'population_%d_%s.png' % (num_cells, celltype)))


def make_synapse(cell, weight, input_idx, input_spike_train, e=0.):

    synapse_parameters = {
        'idx': input_idx,
        'e': e,
        'syntype': 'Exp2Syn',
        # 'tau': 2.,
        'tau1' : 1.,                #Time constant, rise
        'tau2' : 3.,                #Time constant, decay
        # 'tau2': 2,
        'weight': weight,
        'record_current': False,
    }
    synapse = LFPy.Synapse(cell, **synapse_parameters)
    synapse.set_spike_times(input_spike_train)
    return cell, synapse


def return_cell(celltype, conductance_type="passive", cell_number=None):
    ### MAKING THE CELL

    np.random.seed((random_seed + random_seed_shift[celltype]) * cell_number)

    if celltype == 'L5':
        morphology_name = '050217-zA.CNG.swc'
        cell_parameters = {
                'morphology': join(root_folder, "morphologies", morphology_name),
                'v_init': -70,
                'passive': True,
                'nsegs_method': "lambda_f",
                "lambda_f": 100,
                'dt': 2**-4,
                'tstart': 0,  # [ms] Simulation start time
                'tstop': 200,  # [ms] Simulation end time
                "pt3d": True,
        }

        cell = LFPy.Cell(**cell_parameters)
        # print cell.totnsegs
        cell.set_rotation(x=np.pi/2, y=-0.05)
        cell = scale_cell(cell, cell_parameters, np.random.normal(1., 0.1))

    elif celltype == 'L23':
        morphology_name = 'Animal-B-IR1-1-b.CNG.swc'
        cell_parameters = {
                'morphology': join(root_folder, "morphologies", morphology_name),
                'v_init': -70,
                'passive': True,
                'nsegs_method': "lambda_f",
                "lambda_f": 100,
                'dt': 2**-4,
                'tstart': 0,  # [ms] Simulation start time
                'tstop': 200,  # [ms] Simulation end time
                "pt3d": True,
        }

        cell = LFPy.Cell(**cell_parameters)
        cell = fix_cell_diam(cell, cell_parameters)
        cell.set_rotation(x=np.pi/2, y=0.1)
        cell = scale_cell(cell, cell_parameters, np.random.normal(0.8, 0.1))
    elif celltype == "human_L23":
        morph_name = "2013_03_13_cell06_945_H42_05.ASC"
        # morph_name = "2013_03_13_cell05_675_H42_04.ASC"
        # morph_name = "2013_03_13_cell03_1204_H42_02.ASC"
        # morph_name = "2013_03_06_cell11_1125_H41_06.ASC"
        # morph_name = "2013_03_06_cell08_876_H41_05_Cell2.ASC"
        model_folder = join(root_folder, "morphologies", "EyalEtAl2016")
        morph_file = join(model_folder, "morphs", morph_name)
        custom_code = [join(model_folder, "PassiveModels", "model_1303_cell06.hoc")]
        cell_parameters = {
                'morphology': morph_file,
                'templatefile': join(model_folder, "PassiveModels", "model_1303_cell06.hoc"),
                'templatename': "model_1303_cell06",
                'templateargs': None,
                'v_init': -70,
                'passive': False,
                'nsegs_method': "lambda_f",
                "lambda_f": 100,
                'dt': 2**-4,
                'tstart': 0,  # [ms] Simulation start time
                'tstop': 200,  # [ms] Simulation end time
                "pt3d": True,
                # "custom_code": custom_code,
                # "remove_axon": True,
        }

        cell = LFPy.TemplateCell(**cell_parameters)
        # cell.set_rotation(x=np.pi/2, y=-np.pi/3) # "2013_03_13_cell05_675_H42_04.ASC"
        cell.set_rotation(x=np.pi/2, y=-np.pi/1.9) # "2013_03_13_cell06_945_H42_05.ASC"
        # cell.set_rotation(x=np.pi/2, y=-np.pi/1.1) # "2013_03_06_cell11_1125_H41_06.ASC"
        # cell.set_rotation(x=np.pi/2, y=np.pi/30) # "2013_03_13_cell03_1204_H42_02.ASC"
        # cell.set_rotation(x=0, y=np.pi/1.2) # "2013_03_06_cell08_876_H41_05_Cell2.ASC"

        cell = scale_cell(cell, cell_parameters, np.random.normal(1., 0.1))

        # plt.close("all")
        # [plt.plot([cell.xstart[idx], cell.xend[idx]], [cell.zstart[idx], cell.zend[idx]], 'k') for idx in range(cell.totnsegs)]
        # plt.axis("equal")
        # plt.savefig("morph_test_%s.png" % morph_name)
        # sys.exit()
    elif celltype == "human_L5":
        morph_name = "2013_03_13_cell06_945_H42_05.ASC"
        # morph_name = "2013_03_13_cell05_675_H42_04.ASC"
        # morph_name = "2013_03_13_cell03_1204_H42_02.ASC"
        # morph_name = "2013_03_06_cell11_1125_H41_06.ASC"
        # morph_name = "2013_03_06_cell08_876_H41_05_Cell2.ASC"
        model_folder = join(root_folder, "morphologies", "EyalEtAl2016")
        morph_file = join(model_folder, "morphs", morph_name)
        custom_code = [join(model_folder, "PassiveModels", "model_1303_cell06.hoc")]
        cell_parameters = {
                'morphology': morph_file,
                'templatefile': join(model_folder, "PassiveModels", "model_1303_cell06.hoc"),
                'templatename': "model_1303_cell06",
                'templateargs': None,
                'v_init': -70,
                'passive': False,
                'nsegs_method': "lambda_f",
                "lambda_f": 100,
                'dt': 2**-4,
                'tstart': 0,  # [ms] Simulation start time
                'tstop': 200,  # [ms] Simulation end time
                "pt3d": True,
                # "custom_code": custom_code,
                # "remove_axon": True,
        }
        cell = LFPy.TemplateCell(**cell_parameters)
        # cell.set_rotation(x=np.pi/2, y=-np.pi/3) # "2013_03_13_cell05_675_H42_04.ASC"
        cell.set_rotation(x=np.pi/2, y=-np.pi/1.9) # "2013_03_13_cell06_945_H42_05.ASC"
        # cell.set_rotation(x=np.pi/2, y=-np.pi/1.1) # "2013_03_06_cell11_1125_H41_06.ASC"
        # cell.set_rotation(x=np.pi/2, y=np.pi/30) # "2013_03_13_cell03_1204_H42_02.ASC"
        # cell.set_rotation(x=0, y=np.pi/1.2) # "2013_03_06_cell08_876_H41_05_Cell2.ASC"
        cell = scale_cell(cell, cell_parameters, np.random.normal(2., 0.1))

        # plt.close("all")
        # [plt.plot([cell.xstart[idx], cell.xend[idx]], [cell.zstart[idx], cell.zend[idx]], 'k') for idx in range(cell.totnsegs)]
        # plt.axis("equal")
        # plt.savefig("morph_test_%s.png" % morph_name)
        # sys.exit()

    else:
        # morphology_name = '050217-zA.CNG.swc'
        labs = ["suter_shepherd", "krieger", "brumberg", "hoffman+helmstaedter"]
        morph_file = None
        for lab in labs:
            morphology_folder = join(root_folder, "morphologies", lab, "CNG version")
            morph_file = join(morphology_folder, celltype)
            if os.path.isfile(morph_file):
                break

        cell_parameters = {
                'morphology': morph_file,
                'v_init': -70,
                'passive': True,
                'nsegs_method': "lambda_f",
                "lambda_f": 100,
                'dt': 2**-4,
                'tstart': 0,  # [ms] Simulation start time
                'tstop': 100,  # [ms] Simulation end time
                "pt3d": True,
                "remove_axon": True,
        }

        cell = LFPy.Cell(**cell_parameters)
        cell = scale_cell(cell, cell_parameters, np.random.normal(1., 0.1))
        cell.set_rotation(x=np.pi/2, y=-0.0)

    if cell_number is not None:
        cell_x_y_z_rotation = np.load(join(root_folder, 'x_y_z_rot_%d_%s.npy' % (10000, celltype)))
        cell.set_rotation(z=cell_x_y_z_rotation[cell_number][3])
        z_shift = np.max(cell.zend) + 10#+ layer_thickness / 2
        # print z_shift, cell_x_y_z_rotation[cell_number][2]
        cell.set_pos(x=cell_x_y_z_rotation[cell_number][0],
                     y=cell_x_y_z_rotation[cell_number][1],
                     z=cell_x_y_z_rotation[cell_number][2] - z_shift)

        # print cell.somapos, np.max(cell.zend)
        if np.max(cell.zend) > 0:
            raise RuntimeError("Cell reaches above cortex")

    return cell


def make_input(cell, input_type, cell_number, celltype=None):

    np.random.seed((random_seed + random_seed_shift[celltype]) * cell_number)

    weight = cell_weights[celltype]

    if input_type == "bombardment":
        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=1000)
        for input_idx in input_idxs:
            input_spike_train = np.array([cell.tstartms + (cell.tstopms - cell.tstartms) * np.random.random()])
            cell, synapse = make_synapse(cell, weight * np.random.normal(1., 0.2), input_idx, input_spike_train)
    elif "single_synapse" in input_type:
        if "apic" in input_type:
            input_idx = cell.get_closest_idx(z=-100, x=-50)
        elif "basal" in input_type:
            input_idx = cell.get_closest_idx(z=cell.zmid[0] - 20, x=-50)
        else:
            raise RuntimeError("Single input where?")
        cell, synapse = make_synapse(cell, weight, input_idx, np.array([5.]))

    elif input_type == "transient_bombardment":

        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=1000)
        for input_idx in input_idxs:
            input_spike_train = np.array([cell.tstartms + (cell.tstopms - cell.tstartms) * np.random.random()])
            cell, synapse = make_synapse(cell, weight/50 * np.random.normal(1., 0.2), input_idx, input_spike_train)

        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=1000)
        for input_idx in input_idxs:
            input_spike_train = np.array([cell.tstartms + (cell.tstopms - cell.tstartms - 100) * np.random.random()])
            cell, synapse = make_synapse(cell, weight/10 * np.random.normal(1., 0.2), input_idx, input_spike_train)

    elif input_type == "pulse":
        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=200, z_min=-100)
        for input_idx in input_idxs:
            input_spike_train = np.array([cell.tstartms + (cell.tstopms - cell.tstartms) * np.random.random()])
            cell, synapse = make_synapse(cell, weight/10 * np.random.normal(1., 0.2), input_idx, input_spike_train)

        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=1000, z_min=-100)#, z_min=200)
        pulse_center = 50 + np.random.normal(0, 1)
        for input_idx in input_idxs:
            input_spike_train = np.random.normal(pulse_center, 10, size=1)
            cell, synapse = make_synapse(cell, weight/3 * np.random.normal(1., 0.2), input_idx, input_spike_train)

    elif input_type == "tuft_pulse":

        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=100, z_min=-200)
        pulse_center = 20 #+ np.random.normal(0, 1)
        for input_idx in input_idxs:
            input_spike_train = np.random.normal(pulse_center, 1.5, size=1)
            cell, synapse = make_synapse(cell, weight/100. * np.random.normal(1., 0.2), input_idx, input_spike_train)

    elif input_type == "basal_pulse":

        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=100,
                                                 z_min=cell.zmid[0] - 100,
                                                 z_max=cell.zmid[0] + 100)

        pulse_center = pulse_centers[layer_dict[celltype]]

        for input_idx in input_idxs:
            input_spike_train = np.random.normal(pulse_center, 1.5, size=1)
            cell, synapse = make_synapse(cell, weight * np.random.normal(1., 0.2), input_idx, input_spike_train)

    elif input_type == "changing_pathways":
        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=100, z_min=-400)
        pulse_center = 10 #+ np.random.normal(0, 1)
        for input_idx in input_idxs:
            input_spike_train = np.random.normal(pulse_center, 3, size=1)
            cell, synapse = make_synapse(cell, weight/100. * np.random.normal(1., 0.2), input_idx, input_spike_train)

        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=100,
                                                 z_min=cell.zmid[0]-200, z_max=cell.zmid[0]+200)
        pulse_center = 60 #+ np.random.normal(0, 1)
        for input_idx in input_idxs:
            input_spike_train = np.random.normal(pulse_center, 3, size=1)
            cell, synapse = make_synapse(cell, weight/100. * np.random.normal(1., 0.2), input_idx, input_spike_train)

        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=200,
                                                 z_max=np.max(cell.zmid))
        pulse_center = 110 #+ np.random.normal(0, 1)
        for input_idx in input_idxs:
            input_spike_train = np.random.normal(pulse_center, 3, size=1)
            cell, synapse = make_synapse(cell, weight/100. * np.random.normal(1., 0.2), input_idx, input_spike_train)

        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=200,
                                                    z_max=np.max(cell.zmid))
        pulse_center = 160 #+ np.random.normal(0, 1)
        for input_idx in input_idxs:
            input_spike_train = np.random.normal(pulse_center, 3, size=1)
            cell, synapse = make_synapse(cell, weight/100. * np.random.normal(1., 0.2), input_idx, input_spike_train)

        input_idxs = cell.get_rand_idx_area_norm(section='allsec', nidx=50,
                                                 z_max=cell.zmid[0]+200)
        pulse_center = 160 #+ np.random.normal(0, 1)
        for input_idx in input_idxs:
            input_spike_train = np.random.normal(pulse_center, 3, size=1)
            cell, synapse = make_synapse(cell, 4*weight/100. * np.random.normal(1., 0.2),
                                         input_idx, input_spike_train, e=-90.)

    else:
        raise RuntimeError("Stimuli not recognized")
    return cell, synapse

def return_electrode_parameters():
    # Making x,y,z coordinates of three electrodes
    elec_z = np.linspace(-2000, 0, 11)#np.linspace(-900, 0, 9)
    elec_x = np.ones(len(elec_z)) * (0)
    elec_y = np.zeros(len(elec_z))
    electrode_parameters = {
        'sigma': 0.3,       # extracellular conductivity
        'x': elec_x,        # x,y,z-coordinates of contact points
        'y': elec_y,
        'z': elec_z,
        'method': 'soma_as_point',
    }
    return electrode_parameters


def scale_cell(cell, cell_parameters, scale_factor):
    """ Scales the cell in z-direction by scale_factor.
    IMPORTANT: pt3d MUST BE TRUE in cell_parameters, or strange stuff happens"
    """
    if "pt3d" not in cell_parameters or not cell_parameters["pt3d"]:
        raise RuntimeError("To scale cell pt3d must be True in cell_parameters")
    i = 0
    for sec in cell.allseclist:
        n3d = int(neuron.h.n3d())
        for n in range(n3d):
            neuron.h.pt3dchange(n,
                            h.x3d(n),
                            h.y3d(n),
                            h.z3d(n) * scale_factor,
                            h.diam3d(n))
        i += 1
    # let NEURON know about the changes we just did:
    neuron.h.define_shape()

    cell.x3d, cell.y3d, cell.z3d, cell.diam3d = cell._collect_pt3d()
    cell._create_sectionlists()
    cell._set_nsegs(cell_parameters['nsegs_method'], cell_parameters['lambda_f'], 0.1, None)
    cell.totnsegs = cell._calc_totnsegs()
    ##must recollect the geometry, otherwise we get roundoff errors!
    cell._collect_geometry()
    cell.x3d, cell.y3d, cell.z3d, cell.diam3d = cell._collect_pt3d()

    return cell


def fix_cell_diam(cell, cell_parameters):

    if "pt3d" not in cell_parameters or not cell_parameters["pt3d"]:
        raise RuntimeError("To scale cell pt3d must be True in cell_parameters")
    i = 0
    for sec in cell.allseclist:
        n3d = int(neuron.h.n3d())
        for n in range(n3d):
            if np.abs(h.diam3d(n) - 0.08) < 1e-7:
                neuron.h.pt3dchange(n,
                                h.x3d(n),
                                h.y3d(n),
                                h.z3d(n),
                                1.0)
        i += 1
    # let NEURON know about the changes we just did:
    neuron.h.define_shape()

    cell.x3d, cell.y3d, cell.z3d, cell.diam3d = cell._collect_pt3d()
    cell._create_sectionlists()
    cell._set_nsegs(cell_parameters['nsegs_method'], cell_parameters['lambda_f'], 0.1, None)
    cell.totnsegs = cell._calc_totnsegs()
    ##must recollect the geometry, otherwise we get roundoff errors!
    cell._collect_geometry()
    cell.x3d, cell.y3d, cell.z3d, cell.diam3d = cell._collect_pt3d()
    return cell


def single_cell_simulation(cell_number=1, celltype="almog",
                           conductance_type="passive",
                           input_type="pulse", plot=False):

    pop_name = 'eeg_pop_%s_%s_%s' % (input_type, celltype, conductance_type)

    fig_name = '%s_%04d' % (pop_name, cell_number)
    # if os.path.isfile(join(root_folder, celltype, "LFPs", "lfp_%s.npy" % fig_name)):
    #     print("Skipping {}".format(fig_name))
    #     return

    cell = return_cell(celltype, conductance_type, cell_number)

    cell, syn_input = make_input(cell, input_type, cell_number, celltype=celltype)

    cell.simulate(rec_imem=True, rec_vmem=True, rec_current_dipole_moment=True)

    electrode_parameters = return_electrode_parameters()

    # plt.close("all")
    # [plt.plot(cell.tvec, cell.vmem[idx, :]) for idx in range(cell.totnsegs)]
    # plt.show()

    electrode = LFPy.RecExtElectrode(cell, **electrode_parameters)
    electrode.calc_lfp()

    LFP = 1000 * electrode.LFP

    radii, sigmas, rad_tol = return_head_parameters()

    P = cell.current_dipole_moment
    somapos = np.array([cell.somapos[0],
                        cell.somapos[1],
                        cell.somapos[2] + radii[0] - 1500])
    # cell.set_pos(x=somapos[0], y=somapos[1], z=somapos[2])
    r_soma_syns = [cell.get_intersegment_vector(idx0=0, idx1=i)
                   for i in cell.synidx]
    r_mid = np.average(r_soma_syns, axis=0)
    r_mid = somapos + r_mid/2.

    if not os.path.isdir(join(root_folder, celltype, "LFPs")):
        os.mkdir(join(root_folder, celltype))
        os.mkdir(join(root_folder, celltype, "LFPs"))

    if not os.path.isdir(join(root_folder, celltype, "EEG")):
        os.mkdir(join(root_folder, celltype, "EEG"))

    np.save(join(root_folder, celltype, "LFPs", "lfp_%s.npy" % fig_name), LFP)
    np.save(join(root_folder, celltype, "EEG", "cdpm_%s.npy" % fig_name), P)
    np.save(join(root_folder, celltype, "EEG", "r_mid_%s.npy" % fig_name), r_mid)

    if cell_number == 0:
        np.save(join(root_folder, celltype, "EEG", "tvec_%s.npy" % pop_name), cell.tvec)
        np.save(join(root_folder, celltype, "LFPs", "tvec_%s.npy" % pop_name), cell.tvec)

    # if plot or not cell_number % 10:
    if plot or not cell_number > 5 or not cell_number % 10:
        print "Vmem range: ", np.min(cell.vmem), np.max(cell.vmem)
        plot_single_cell_LFP(cell, electrode_parameters, P, r_mid,
                             celltype, fig_name)


def return_head_parameters():
    radii = [79000., 80000., 85000., 90000.]
    sigmas = [0.3, 1.5, 0.015, 0.3]
    rad_tol = 1e-2
    return radii, sigmas, rad_tol

def return_eeg_sphere_coordinates():
    #measurement points
    # for nice plot use theta_step = 1 and phi_step = 1. NB: Long computation time.
    theta_step = 4
    phi_step = 4
    radii, sigmas, rad_tol = return_head_parameters()
    theta, phi_angle = np.mgrid[0.:180.:theta_step, 0.:360.+phi_step:phi_step]

    num_theta = theta.shape[0]
    num_phi = theta.shape[1]
    theta = theta.flatten()
    phi_angle = phi_angle.flatten()

    theta_r = np.deg2rad(theta)
    phi_angle_r = np.deg2rad(phi_angle)

    x_eeg = (radii[3] - rad_tol) * np.sin(theta_r) * np.cos(phi_angle_r)
    y_eeg = (radii[3] - rad_tol) * np.sin(theta_r) * np.sin(phi_angle_r)
    z_eeg = (radii[3] - rad_tol) * np.cos(theta_r)
    eeg_coords = np.vstack((x_eeg, y_eeg, z_eeg)).T

    return eeg_coords, num_theta, num_phi

def plot_EEG(fig, cell, P, r_mid):

    radii, sigmas, rad_tol = return_head_parameters()

    eeg_coords_top = np.array([[0., 0., radii[3] - rad_tol]])
    four_sphere_top = LFPy.FourSphereVolumeConductor(radii, sigmas, eeg_coords_top, r_mid)
    pot_db_4s_top = four_sphere_top.calc_potential(P)
    eeg_top = np.array(pot_db_4s_top) * 1e9

    ax = fig.add_subplot(248, title="EEG at top", ylabel='pA', xlabel='Time (ms)')
    ax.plot(cell.tvec, eeg_top[0], 'k')
    simplify_axes(ax)

    from mpl_toolkits.mplot3d import Axes3D

    eeg_coords, num_theta, num_phi = return_eeg_sphere_coordinates()

    # potential in 4S with db
    time_max = np.argmax(np.linalg.norm(P, axis=1))
    p = P[time_max, None]#.reshape(1,3) # picked out a single timepoint to make it go faster, but you can insert more dipoles here. They will need to have the same location, though.
    four_sphere = LFPy.FourSphereVolumeConductor(radii, sigmas, eeg_coords, r_mid)
    pot_db_4s = four_sphere.calc_potential(p)
    eeg = pot_db_4s.reshape(num_theta, num_phi)*1e9# from mV to pV

    ax = fig.add_subplot(244, projection='3d', title="EEG at t={}".format(cell.tvec[time_max]))
    vmax = 2
    vmin = -vmax
    clr = lambda phi: plt.cm.PRGn((phi - vmin) / (vmax - vmin))
    clrs = clr(eeg)
    surf = ax.plot_surface(eeg_coords[:, 0].reshape(num_theta, num_phi),
                           eeg_coords[:, 1].reshape(num_theta, num_phi),
                           eeg_coords[:, 2].reshape(num_theta, num_phi),
                           rstride=1, cstride=1, facecolors=clrs,
                           linewidth=0, antialiased=False)

    ax.set_aspect('equal')
    ax.axis('off')
    ax.set_xlim3d(-65000, 65000)
    ax.set_ylim3d(-65000, 65000)
    ax.set_zlim3d(-65000, 65000)
    ax.view_init(10, 0)

    # colorbar
    cax = fig.add_axes([0.65, 0.75, 0.25, 0.01])
    m = plt.cm.ScalarMappable(cmap=plt.cm.PRGn)
    ticks = np.linspace(vmin, vmax, 5) # global normalization
    m.set_array(ticks)
    cbar = fig.colorbar(m, cax=cax, #format='%3.6f',
                        extend='both', orientation='horizontal')
    cbar.outline.set_visible(False)
    cbar.set_ticks(ticks)
    #unit is pV
    # cax.set_xticklabels([format(t, '3.1f') for t in ticks])
    cbar.set_label(r'$\phi$ (pV)', labelpad=1.)

def plot_single_cell_LFP(cell, electrode_parameters, P, r_mid, celltype, fig_name):

    electrode = LFPy.RecExtElectrode(cell, **electrode_parameters)
    electrode.calc_lfp()

    LFP = 1000. * (electrode.LFP - electrode.LFP[:, 0, None])

    elec_idx_colors = {idx: plt.cm.Reds_r(1./(len(electrode.x) + 1) * idx) for idx in range(len(electrode.x))}

    plt.close('all')
    fig = plt.figure(figsize=(16, 9))

    plot_EEG(fig, cell, P, r_mid)
    plt.subplots_adjust(hspace=0.5, wspace=0.5)

    xmax = np.max([np.max(np.abs(cell.xend)), 200])

    ax_sp = plt.subplot(141, xlabel='Time [ms]', ylabel='y [$\mu$m]',
                        title="Single-cell input spike-trains",
                        ylim=[-2100, 100], yticks=[-2000, -1500, -1000, -500, 0],
                       xlim=[0, cell.tvec[-1]], frameon=False)

    ax_m = plt.subplot(142,  xlabel='x [$\mu m$]', ylabel='y [$\mu m$]',
                       ylim=[-2100, 100], xlim=[-xmax, xmax], sharey=ax_sp,
                       xticks=[], frameon=False,
                       yticks=[-2000, -1500, -1000, -500, 0])

    ax_lfp = plt.subplot(143, frameon=False,
                         yticks=[-2000, -1500, -1000, -500, 0],
                         xticks=[], sharey=ax_m,
                         title="LFP")

    [ax_m.plot([cell.xstart[idx], cell.xend[idx]],
               [cell.zstart[idx], cell.zend[idx]], c='k', clip_on=False)
     for idx in xrange(cell.totnsegs)]
    ax_m.plot(cell.xmid[0], cell.zmid[0], 'o', c="k", ms=12)

    [ax_m.plot(electrode.x[idx], electrode.z[idx], 'D',  c=elec_idx_colors[idx], ms=8, clip_on=False)
     for idx in xrange(len(electrode.x))]

    for syn_number, syn in enumerate(cell.synapses):

        c = 'r' if syn.kwargs["e"] > -60 else 'b'

        ax_sp.plot(cell.sptimeslist[syn_number],
                   np.ones(len(cell.sptimeslist[syn_number])) * cell.zmid[syn.idx], '.', c=c)
        ax_m.plot(cell.xmid[syn.idx], cell.zmid[syn.idx], '*', c=c, ms=7, clip_on=False)

    # for key, lims in layer_thickness_dict.items():

        # ax_m.axhline(lims[1], ls="--", c='gray')
        # ax_m.text(ax_m.get_xlim()[0], lims[1], key, ha="left", va="bottom")
        # ax_sp.axhline(lims[1], ls="--", c='gray')
        # ax_sp.text(ax_sp.get_xlim()[0], lims[1], key, ha="left", va="bottom")


    dz = electrode.z[1] - electrode.z[0]

    vmax = 0.05#np.max(np.abs(LFP)) / 10
    vmin = -vmax
    img = ax_lfp.imshow(LFP, extent=[0, cell.tvec[-1], np.min(electrode.z) - dz/2, np.max(electrode.z) + dz/2],
                        interpolation='nearest', origin='lower', cmap=plt.cm.coolwarm, vmin=vmin, vmax=vmax)

    cbar = plt.colorbar(img, shrink=0.5, label='$\mu$V')

    normalize = np.max(np.abs((LFP[:, :] - LFP[:, 0, None])))
    for idx in range(len(electrode.z)):
        y = electrode.z[idx] + (LFP[idx] - LFP[idx, 0]) / normalize * dz
        ax_lfp.plot(cell.tvec, y, lw=1, c='k', clip_on=False)

    ax_lfp.plot([cell.tvec[-1] - 10, cell.tvec[-1]],
                [np.min(electrode.z) - 20, np.min(electrode.z) - 20], lw=4, c='k', clip_on=False)
    ax_lfp.text(cell.tvec[-1] - 10, np.min(electrode.z) - 50, '10 ms')
    ax_lfp.axis("auto")

    plt.savefig(join(root_folder, celltype, '%s.png' % fig_name))


if __name__ == '__main__':

    # conductance_type = "passive"

    if len(sys.argv) == 1:
        celltype = ['L23', "L5", "human_L23", "human_L5"][3]
        input_type = ["basal_pulse", "changing_pathways"][1]
        # initialize_population(10000, celltype)
        single_cell_simulation(celltype=celltype, input_type=input_type,
                               cell_number=0, plot=True,
                               conductance_type="passive")
    else:
        single_cell_simulation(
                            celltype=sys.argv[1],
                            input_type=sys.argv[2],
                            cell_number=int(sys.argv[4]),
                            conductance_type=sys.argv[3])

import os
import sys
import numpy as np
from os.path import join
import matplotlib
# matplotlib.use("AGG")
import pylab as plt
from plotting_convention import *
from main import (return_cell, return_electrode_parameters, make_input,
                  random_seed, random_seed_shift, return_head_parameters,
                  return_eeg_sphere_coordinates)

import LFPy
from matplotlib import ticker

root_folder = ".."

def plot_num_cells_to_ax(num_cells, ax_m, ax_sp,
                         celltype, electrode, input_type, tot_num_cells,
                         cmap=plt.cm.viridis):

    cell_idx_colors = lambda idx: cmap(1./(tot_num_cells + 1) * idx)

    step = int(tot_num_cells / num_cells)

    for cell_number in range(tot_num_cells)[::step]:
        plt.seed((random_seed + random_seed_shift[celltype]) * cell_number)
        c = cell_idx_colors(np.random.randint(0, tot_num_cells))
        cell = return_cell(celltype=celltype, conductance_type="passive", cell_number=cell_number)

        for idx in xrange(cell.totnsegs):
            if idx == 0:
                ax_m.plot(cell.xmid[idx], cell.zmid[idx], 'o', ms=12,
                           c=c, zorder=0, rasterized=True)
            else:
                ax_m.plot([cell.xstart[idx], cell.xend[idx]], [cell.zstart[idx], cell.zend[idx]],
                           c=c, zorder=0, rasterized=True)
        # [plt.plot(cell.xmid[idx], cell.zmid[idx], 'o', c=cell_idx_colors[idx], ms=12) for idx in cell_plot_idxs]
        # [ax.plot(cell.xmid[idx], cell.zmid[idx], 'o', c='orange', ms=2) for idx in cell.synidx]
        if cell_number == 0:
            cell, syn = make_input(cell, input_type, cell_number, celltype=celltype)
            cell.simulate(rec_imem=True, rec_vmem=True)

            # dz = electrode.z[1] - electrode.z[0]

            for syn_number, syn in enumerate(cell.synapses):
                if cmap is not plt.cm.viridis:
                    color = cmap(0.5)
                    if cell.synapses[syn_number].kwargs["e"] > -60:
                        marker = '.'
                        mec = 'none'
                    else:
                        marker = 'D'
                        mec = 'k'
                else:
                    color = 'r' if cell.synapses[syn_number].kwargs["e"] > -60 else 'b'
                    marker = '.'
                    mec = 'none'


                ax_sp.plot(cell.sptimeslist[syn_number],
                           np.ones(len(cell.sptimeslist[syn_number])) * cell.zmid[syn.idx],
                           marker=marker, color=color, mec=mec)

    if electrode is not None:
        [ax_m.plot(electrode.x[idx], electrode.z[idx], 'D',  c="gray", ms=8, clip_on=False)
            for idx in xrange(len(electrode.x))]


def plot_combined_population_LFP(conductance_type, cell_names, input_type, num_cells):

    electrode_parameters = return_electrode_parameters()
    electrode = LFPy.RecExtElectrode(**electrode_parameters)

    fig_name = 'compare_populations_L23_L5_%s_%s_%d' % (input_type, conductance_type, num_cells)

    LFP = {}

    scale_factors = {"human_L5": 1.0,
                     "human_L23": 1.0,
                     }

    for cell_name in cell_names:
        data_file_name = 'summed_lfp_eeg_pop_%s_%s_%s' % (input_type, cell_name, conductance_type)
        LFP[cell_name] = scale_factors[cell_name] * np.load(join(root_folder, cell_name, "LFPs", "%s_%d.npy" % (data_file_name, num_cells)))
        LFP[cell_name] = LFP[cell_name] - LFP[cell_name][:, 0, None]

    timeres = 2**-4
    tvec = np.arange(LFP[cell_names[0]].shape[1]) * timeres
    LFP["sum"] = np.zeros(LFP[cell_names[0]].shape)
    for cell_name in cell_names:
        LFP["sum"] += LFP[cell_name]

    plt.close('all')
    fig = plt.figure(figsize=(19, 10))


    ylim = [np.min(electrode.z) - 500, 100]

    plt.subplots_adjust(hspace=0.5, wspace=0.5, left=0.075, right=0.95, top=0.90)

    ax_sp = plt.subplot(141,  xlabel='Time [ms]', ylabel='y [$\mu$m]', title="Single-cell input spike-trains",
                       xlim=[0, tvec[-1]], ylim=ylim, frameon=False)

    ax_m = plt.subplot(142, aspect='equal', xlabel='x [$\mu$m]', ylabel='y [$\mu$m]',
                       title="Cell populations (%d cells each)\nand electrodes" % num_cells,
                        ylim=ylim, xticks=[], frameon=False, rasterized=True)

    # ax_vsd = plt.subplot(134, frameon=False, ylabel='y [$\mu$m]',
    #                      yticks=[-800, -600, -400, -200, 0], ylim=[-900, 100], xticks=[],
    #                      title="Single-cell\ndepth-resolved\nmembrane potential")

    cmaps = [plt.cm.Reds, plt.cm.Blues]

    for idx, cell_name in enumerate(cell_names):
        cmap = cmaps[idx]
        plot_num_cells_to_ax(2, ax_m, ax_sp, cell_name, electrode, input_type, num_cells, cmap=cmap)

    mark_subplots([ax_sp], "A", xpos=-0.0, ypos=1.05)
    mark_subplots([ax_m], "B", xpos=-0.7, ypos=1.0)

    ax_lfp = plt.subplot(143, title="Population extracellular potentials",
                         frameon=False, ylabel='y [$\mu$m]',
                         xlabel='Time [ms]', xlim=[0, tvec[-1]],
                         ylim=ylim)#, sharex=ax_e2)

    ax_lfp_comb = plt.subplot(144, title="Population extracellular potentials",
                         frameon=False, ylabel='y [$\mu$m]',
                         xlabel='Time [ms]', xlim=[0, tvec[-1]],
                         ylim=ylim)#, sharex=ax_e2)

    vmax = 1#np.max(np.abs(LFP)) #/ 5
    vmin = -vmax
    dz = electrode.z[1] - electrode.z[0]

    img_dict = {"extent": [0, tvec[-1], np.min(electrode.z) - dz/2,
                           np.max(electrode.z) + dz/2],
                "interpolation": 'nearest',
                "origin": 'lower',
                "cmap": plt.cm.coolwarm_r}

    # img = ax_lfp.imshow(LFP, vmin=vmin, vmax=vmax, **img_dict)#, aspect=0.05)
    normalize = np.max(np.abs((LFP["sum"][:, :] - LFP["sum"][:, 0, None])))

    for idx, cell_name in enumerate(cell_names):
        color = cmaps[idx](.8)

        for idx in range(len(electrode.z)):
            y = electrode.z[idx] + (LFP[cell_name][idx] - LFP[cell_name][idx, 0]) / normalize * dz
            ax_lfp.plot(tvec, y, lw=2, c=color, clip_on=False)

    color = "k"

    for idx in range(len(electrode.z)):
        y = electrode.z[idx] + (LFP["sum"][idx] - LFP["sum"][idx, 0]) / normalize * dz
        ax_lfp_comb.plot(tvec, y, lw=2, c=color, clip_on=False)

    ax_lfp.plot([tvec[-1], tvec[-1]], [-dz, 0], color="k", lw=4, clip_on=False)
    ax_lfp.text(tvec[-1] + 2, -dz/2, "%1.2f $\mu$V" % normalize)

    simplify_axes(fig.axes)

    mark_subplots([ax_lfp], "D", xpos=-0.1, ypos=1.1)
    if not os.path.isdir(cell_name):
        os.mkdir(cell_name)
    plt.savefig(join(root_folder, '%s.png' % fig_name))


def plot_combined_population_EEG(conductance_type, cell_names, input_type, num_cells):

    fig_name = 'compare_populations_EEG_L23_L5_%s_%s_%d' % (input_type, conductance_type, num_cells)

    EEG = {}
    scale_factors = {"human_L5": 1.0,
                     "human_L23": 1.0,
                     }

    for cell_name in cell_names:
        data_file_name = 'summed_eeg_top_eeg_pop_%s_%s_%s' % (input_type, cell_name, conductance_type)
        EEG[cell_name] = scale_factors[cell_name] * np.load(join(root_folder, cell_name, "EEG", "%s_%d.npy" % (data_file_name, num_cells)))
        # EEG[cell_name] = EEG[cell_name] - EEG[cell_name][:, 0, None]

    tvec = np.load(join(root_folder, cell_name, "EEG",
            "tvec_eeg_pop_{}_{}_{}.npy".format(input_type, cell_name, conductance_type)))

    EEG["sum"] = np.zeros(EEG[cell_names[0]].shape)
    for cell_name in cell_names:
        EEG["sum"] += EEG[cell_name]

    plt.close('all')
    fig = plt.figure(figsize=(19, 10))

    ylim = [-3000, 100]

    plt.subplots_adjust(hspace=0.5, wspace=0.5, left=0.075, right=0.95, top=0.90)

    ax_sp = plt.subplot(131,  xlabel='Time [ms]', ylabel='y [$\mu$m]', title="Single-cell input spike-trains\nColors corespond to populations\nExcitatory: dots; Inhibitory: diamonds",
                       xlim=[0, tvec[-1]], ylim=ylim, frameon=False)

    ax_m = plt.subplot(132, aspect='equal', xlabel='x [$\mu$m]', ylabel='y [$\mu$m]',
                       title="Cell populations (%d cells each)\nand electrodes" % num_cells,
                        ylim=ylim, xticks=[], frameon=False, rasterized=True)

    cmaps = [plt.cm.Greens, plt.cm.Blues]

    for idx, cell_name in enumerate(cell_names):
        cmap = cmaps[idx]
        plot_num_cells_to_ax(100, ax_m, ax_sp, cell_name, None, input_type, num_cells, cmap=cmap)

    mark_subplots([ax_sp], "A", xpos=-0.01, ypos=1.05)
    mark_subplots([ax_m], "B", xpos=-0.2, ypos=1.0)

    ax_eeg = plt.subplot(133, title="Population EEGs at top of head", ylabel='nV',
                         xlabel='Time [ms]', xlim=[0, tvec[-1]])#, sharex=ax_e2)
    name_dict = {"human_L23": "L23",
                 "human_L5": "L5"}
    lines = []
    line_names = []
    for idx, cell_name in enumerate(cell_names):
        color = cmaps[idx](.8)
        l, = ax_eeg.plot(tvec, EEG[cell_name] / 1000, lw=2, c=color, clip_on=False)

        lines.append(l)
        line_names.append(name_dict[cell_name])
    color = "k"
    l_sum, = ax_eeg.plot(tvec, EEG["sum"] / 1000, lw=3, c=color, clip_on=False)
    lines.append(l_sum)
    line_names.append("Sum")


    ax_eeg.legend(lines, line_names, frameon=False, loc="lower right")
    simplify_axes(fig.axes)

    mark_subplots([ax_eeg], "C", xpos=-0.1, ypos=1.05)
    if not os.path.isdir(cell_name):
        os.mkdir(cell_name)
    plt.savefig(join(root_folder, '%s.png' % fig_name))



def plot_EEG_sphere(fig, tvec, P, r_mid, input_wave_idxs, wave_clrs):

    radii, sigmas, rad_tol = return_head_parameters()

    from mpl_toolkits.mplot3d import Axes3D

    eeg_coords, num_theta, num_phi = return_eeg_sphere_coordinates()
    fig.text(0.73, 0.22, "G", fontweight='demibold', fontsize=12)
    # time_max = np.argmax(np.linalg.norm(P, axis=1))
    # fig.text(0.85, 0.92, "EEG", fontsize=15)
    for num, time_idx in enumerate(input_wave_idxs):
        # potential in 4S with db
        p = P[time_idx, None]#.reshape(1,3) # picked out a single timepoint to make it go faster, but you can insert more dipoles here. They will need to have the same location, though.
        four_sphere = LFPy.FourSphereVolumeConductor(radii, sigmas, eeg_coords, r_mid)
        pot_db_4s = four_sphere.calc_potential(p)
        eeg = pot_db_4s.reshape(num_theta, num_phi)*1e9# from mV to pV

        ax = fig.add_axes([0.74 + 0.06*num, 0.10, 0.05, 0.1], projection='3d',)

        ax.set_title("{0:.0f} ms".format(tvec[time_idx]), color=wave_clrs[num])
        vmax = 2
        vmin = -vmax
        clr = lambda phi: plt.cm.PRGn((phi - vmin) / (vmax - vmin))
        clrs = clr(eeg)
        surf = ax.plot_surface(eeg_coords[:, 0].reshape(num_theta, num_phi),
                               eeg_coords[:, 1].reshape(num_theta, num_phi),
                               eeg_coords[:, 2].reshape(num_theta, num_phi),
                               rstride=1, cstride=1, facecolors=clrs,
                               linewidth=0, antialiased=False)

        ax.set_aspect('equal')
        ax.axis('off')
        ax.set_xlim3d(-65000, 65000)
        ax.set_ylim3d(-65000, 65000)
        ax.set_zlim3d(-65000, 65000)
        ax.view_init(10, 0)

        if num == 6:
            # colorbar
            cax = fig.add_axes([0.75, 0.5, 0.2, 0.01])
            m = plt.cm.ScalarMappable(cmap=plt.cm.PRGn)
            ticks = np.linspace(vmin, vmax, 5) # global normalization
            m.set_array(ticks)
            cbar = fig.colorbar(m, cax=cax, #format='%3.6f',
                                extend='both', orientation='horizontal')
            cbar.outline.set_visible(False)
            cbar.set_ticks(ticks)
            #unit is pV
            # cax.set_xticklabels([format(t, '3.1f') for t in ticks])
            cbar.set_label(r'$\phi$ (pV)', labelpad=1.)


def plot_population_eeg(conductance_type, cell_name, input_type, num_cells):

    electrode_parameters = return_electrode_parameters()
    electrode = LFPy.RecExtElectrode(**electrode_parameters)

    pop_name = 'eeg_pop_%s_%s_%s' % (input_type, cell_name, conductance_type)

    fig_name = 'summed_lfp_eeg_pop_%s_%s_%s' % (input_type, cell_name, conductance_type)
    eeg_name = 'summed_eeg_top_eeg_pop_%s_%s_%s' % (input_type, cell_name, conductance_type)
    P_name = 'average_P_eeg_pop_%s_%s_%s' % (input_type, cell_name, conductance_type)
    r_mid_name = 'average_r_mid_eeg_pop_%s_%s_%s' % (input_type, cell_name, conductance_type)

    # LFP = np.load(join(root_folder, cell_name, "LFPs", "%s_%d.npy" % (fig_name, num_cells)))
    # LFP = LFP - LFP[:, 0, None]

    eeg_top = np.load(join(root_folder, cell_name, "EEG", "%s_%d.npy" % (eeg_name, num_cells)))

    avrg_P = np.load(join(root_folder, cell_name, "EEG", "%s_%d.npy" % (P_name, num_cells)))
    avrg_r_mid = np.load(join(root_folder, cell_name, "EEG", "%s_%d.npy" % (r_mid_name, num_cells)))

    tvec = np.load(join(root_folder, cell_name, "EEG", "tvec_%s.npy" % pop_name))

    radii, sigmas, rad_tol = return_head_parameters()

    eeg_coords_top = np.array([[0., 0., radii[3] - rad_tol]])
    four_sphere_top = LFPy.FourSphereVolumeConductor(radii, sigmas, eeg_coords_top, avrg_r_mid)
    pot_db_4s_top = four_sphere_top.calc_potential(avrg_P)
    avrg_eeg_top = np.array(pot_db_4s_top)[0] * 1e9 * num_cells

    # plt.plot(avrg_P[:, 2])
    # plt.plot(eeg_top / 1000)
    # plt.plot(avrg_eeg_top / 1000)
    # plt.show()

    wave_clrs = ["y", "c", "g", "pink"]

    input_wave_idxs = [np.argmin(eeg_top), np.argmax(eeg_top),
                       np.argmin(np.abs(113 - tvec)),
                       1000 + np.argmin(eeg_top[1000:])]

    plt.close('all')
    fig = plt.figure(figsize=(19, 10))

    plot_EEG_sphere(fig, tvec, avrg_P, avrg_r_mid, input_wave_idxs, wave_clrs)

    fig.subplots_adjust(hspace=0.6, wspace=0.5, left=0.075, right=0.95, top=0.90)

    ax_sp = plt.subplot(141,  xlabel='Time [ms]', ylabel='y [$\mu$m]',
                        title="Example of single-cell\ninput spike-trains",
                       xlim=[0, tvec[-1]], ylim=[np.min(electrode.z), 100], frameon=False)

    ax_m = plt.subplot(142, aspect='equal', xlabel='x [$\mu$m]', ylabel='y [$\mu$m]',
                       title="Cell population (%d cells)\nand laminar electrodes" % num_cells,
                        ylim=[np.min(electrode.z), 100], xlim=[-200, 200],
                       xticks=[], frameon=False, rasterized=True)

    # ax_lfp = plt.subplot(143, title="Population Laminar LFP", frameon=False, ylabel='y [$\mu$m]',
    #                      xlabel='Time [ms]', xlim=[0, tvec[-1]], ylim=[np.min(electrode.z), 100])#, sharex=ax_e2)


    ax_eeg_top = plt.axes([0.76, 0.3, 0.2, 0.6], title="EEG at top of head",
                                     ylabel='nV',
                                     xlabel='Time [ms]', xlim=[0, tvec[-1]])

    plot_num_cells_to_ax(100, ax_m, ax_sp, cell_name, electrode, input_type, num_cells)

    mark_subplots([ax_sp], "A", xpos=-0.0, ypos=1.05)
    mark_subplots([ax_m], "B", xpos=-0.3, ypos=1.05)

    # dz = electrode.z[1] - electrode.z[0]
    # normalize = np.max(np.abs((LFP[:, :] - LFP[:, 0, None])))
    # for idx in range(len(electrode.z)):
    #     y = electrode.z[idx] + (LFP[idx] - LFP[idx, 0]) / normalize * dz
    #     ax_Px.plot(tvec, y, lw=1, c='k', clip_on=False)
    #
    # ax_Px.plot([tvec[-1], tvec[-1]], [-dz, 0], 'k', lw=4, clip_on=False)
    # ax_Px.text(tvec[-1] + 2, -dz/2, "%1.2f $\mu$V" % normalize)

    l1, = ax_eeg_top.plot(tvec, eeg_top / 1000, 'gray', lw=2)
    l2, = ax_eeg_top.plot(tvec, avrg_eeg_top / 1000, 'k--', lw=2)

    for num, idx in enumerate(input_wave_idxs):
        ax_eeg_top.axvline(tvec[idx], ls=':', c=wave_clrs[num])
        # ax_Px.axvline(tvec[idx], ls=':', c=wave_clrs[num])
        ax_sp.axvline(tvec[idx], ls=':', c=wave_clrs[num])

    ax_eeg_top.legend([l1, l2], ["Sum of dipoles", "Average dipole"], frameon=False)

    # ax_P = plt.subplot(344, title="Average current dipole moment",
    #                                  xlabel='Time [ms]', xlim=[0, tvec[-1]])


    ax_Px = plt.subplot(3, 4, 3, title="Current-dipole moment\nP$_X$", ylabel='nA$\mu$m',
                         xlabel='Time [ms]', xlim=[0, tvec[-1]], )

    ax_Py = plt.subplot(3, 4, 7, title="Current-dipole moment\nP$_Y$", ylabel='nA$\mu$m',
                         xlabel='Time [ms]', xlim=[0, tvec[-1]], )

    ax_Pz = plt.subplot(3, 4, 11, title="Current-dipole moment\nP$_Z$", ylabel='nA$\mu$m',
                         xlabel='Time [ms]', xlim=[0, tvec[-1]], )

    lx, = ax_Px.plot(tvec, avrg_P[:, 0], c='k', lw=2)
    ly, = ax_Py.plot(tvec, avrg_P[:, 1], c='k', lw=2)
    lz, = ax_Pz.plot(tvec, avrg_P[:, 2], c='k', lw=2)
    # ax_Pz.legend([lx, ly, lz], ["P$_x$", "P$_y$", "P$_z$"], frameon=False)

    simplify_axes([ax_eeg_top, ax_Px, ax_Py, ax_Pz])
    mark_subplots([ax_Px,  ax_Py, ax_Pz, ax_eeg_top], "CDEF", xpos=-0.1, ypos=1.1)
    if not os.path.isdir(cell_name):
        os.mkdir(cell_name)
    plt.savefig(join(root_folder, 'simple_%s.png' % fig_name))


def sum_and_remove(conductance_type, celltype, input_type,
                   num_cells, remove=False):
    print "summing LFP and removing single LFP files"
    import time
    summed_LFP = np.zeros((0,0))

    pop_name = 'eeg_pop_%s_%s_%s' % (input_type, celltype, conductance_type)
    for cell_number in range(0, num_cells):
        fig_name = '%s_%04d' % (pop_name, cell_number)
        # fig_name = 'eeg_pop_%s_%s_%s_%04d' % (input_type, celltype, conductance_type, cell_idx)
        loaded = False
        t0 = time.time()
        while not loaded:
            try:
                LFP = np.load(join(root_folder, celltype, "LFPs", "lfp_%s.npy" % fig_name))
                loaded = True
            except IOError:
                print cell_number, time.time() - t0
                time.sleep(1)
            if time.time() - t0 > 60:
                print "waited for a minute for %s. Can wait no longer" % fig_name
                return False

        if summed_LFP.shape != LFP.shape:
            summed_LFP = LFP
        else:
            summed_LFP += LFP
    np.save(join(root_folder, celltype, "LFPs", "summed_lfp_%s_%d.npy" % (pop_name, num_cells)), summed_LFP)
    if remove:
        fig_name = join(root_folder, celltype, "LFPs", "lfp_%s_*.npy" % pop_name)
        print "Remove files %s" % fig_name
        os.system("rm %s" % fig_name)
    return True
    # for cell_number in range(0, num_cells):
    #     fig_name = '%s_%04d' % (pop_name, cell_number)
    #     os.system("rm %s" % join(root_folder, celltype, "LFPs", "lfp_%s.npy" % fig_name))



def sum_eeg(input_types, celltypes, conductance_type, num_cells):

    from main import return_head_parameters, return_eeg_sphere_coordinates

    eeg_coords, num_theta, num_phi = return_eeg_sphere_coordinates()
    radii, sigmas, rad_tol = return_head_parameters()

    for cell_name in celltypes:
        for input_type in input_types:

            pop_name = 'eeg_pop_%s_%s_%s' % (input_type, cell_name, conductance_type)
            t = np.load(join(root_folder, cell_name, "EEG", "tvec_%s.npy" % pop_name))
            num_tsteps = len(t)
            summed_eeg_top = np.zeros(num_tsteps)
            summed_eeg = np.zeros((num_theta, num_phi, num_tsteps))

            average_P = np.zeros((num_tsteps, 3))
            average_r_mid = np.zeros(3)
            plt.close("all")
            fig_P = plt.figure(figsize=[6, 10])
            fig_P.subplots_adjust(bottom=0.15, top=0.9, hspace=0.6, wspace=0.6,
                                  left=0.17)
            fig_P.suptitle("Current dipole moments")
            ax_x = fig_P.add_subplot(411, title="Px")
            ax_y = fig_P.add_subplot(412, title="Py")
            ax_z = fig_P.add_subplot(413, title="Pz", xlabel="Time (ms)")
            ax_xy = fig_P.add_subplot(427, title="Position", xlabel="x ($\mu$m)",
                                      ylabel="y ($\mu$m)")
            ax_xz = fig_P.add_subplot(428, title="Position", xlabel="x ($\mu$m)",
                                      ylabel="z ($\mu$m)")

            l1 = None
            for cell_idx in range(num_cells):
                fig_name = 'eeg_pop_%s_%s_%s_%04d' % (input_type, cell_name,
                                                      conductance_type, cell_idx)

                print cell_name, input_type, conductance_type, cell_idx
                P = np.load(join(root_folder, cell_name, "EEG", "cdpm_%s.npy" % fig_name))
                r_mid = np.load(join(root_folder, cell_name, "EEG", "r_mid_%s.npy" % fig_name))
                average_r_mid += r_mid
                average_P += P

                ax_x.plot(t, P[:,0], lw=0.5, c='gray')
                ax_y.plot(t, P[:,1], lw=0.5, c='gray')
                l1, = ax_z.plot(t, P[:,2], lw=0.5, c='gray')
                ax_xy.plot(r_mid[0], r_mid[1], '.', c='gray', ms=10)
                ax_xz.plot(r_mid[0], r_mid[2], '.', c='gray', ms=10)
                eeg_coords_top = np.array([[0., 0., radii[3] - rad_tol]])
                four_sphere_top = LFPy.FourSphereVolumeConductor(radii, sigmas,
                                                                 eeg_coords_top, r_mid)
                pot_db_4s_top = four_sphere_top.calc_potential(P)
                eeg_top = np.array(pot_db_4s_top) * 1e9

                four_sphere = LFPy.FourSphereVolumeConductor(radii, sigmas, eeg_coords, r_mid)
                pot_db_4s = four_sphere.calc_potential(P)
                eeg = pot_db_4s.reshape(num_theta, num_phi, num_tsteps)*1e9# from mV to pV
                # eeg = pot_db_4s.reshape(num_theta, num_phi, )*1e9# from mV to pV

                summed_eeg_top += eeg_top[0, :]
                summed_eeg += eeg

            pop_name = 'eeg_pop_%s_%s_%s_%d' % (input_type, cell_name, conductance_type, num_cells)

            average_r_mid /= num_cells
            average_P /= num_cells

            l2, = ax_x.plot(t, average_P[:, 0], lw=2, c='k')
            ax_y.plot(t, average_P[:, 1], lw=2, c='k')
            ax_z.plot(t, average_P[:, 2], lw=2, c='k')
            ax_xy.plot(average_r_mid[0], average_r_mid[1], '.', c='k', ms=15)
            ax_xz.plot(average_r_mid[0], average_r_mid[2], '.', c='k', ms=15)

            fig_P.legend([l1, l2], ["Single cell", "Population average"],
                         frameon=False, ncol=2, loc="lower center")
            fig_P.savefig(join("..", "all_cdms_{}.png".format(pop_name)))

            np.save(join(root_folder, cell_name, "EEG", "summed_eeg_top_%s.npy" % pop_name), summed_eeg_top)
            np.save(join(root_folder, cell_name, "EEG", "summed_eeg_%s.npy" % pop_name), summed_eeg)

            np.save(join(root_folder, cell_name, "EEG", "average_P_%s.npy" % pop_name), average_P)
            np.save(join(root_folder, cell_name, "EEG", "average_r_mid_%s.npy" % pop_name), average_r_mid)


def plot_eeg_sphere(celltypes, input_types, conductance_type, num_cells):

    from main import return_head_parameters, return_eeg_sphere_coordinates
    from mpl_toolkits.mplot3d import Axes3D

    eeg_coords, num_theta, num_phi = return_eeg_sphere_coordinates()

    for cell_name in celltypes:
        for input_type in input_types:
            pop_name = 'eeg_pop_%s_%s_%s' % (input_type, cell_name, conductance_type)
            fig_name = 'eeg_pop_%s_%s_%s_%d' % (input_type, cell_name, conductance_type, num_cells)

            tvec = np.load(join(root_folder, cell_name, "EEG", "tvec_%s.npy" % pop_name))
            num_tsteps = len(tvec)

            summed_eeg = np.load(join(root_folder, cell_name, "EEG", "summed_eeg_%s.npy" % fig_name))

            mask = np.isnan(summed_eeg)
            summed_eeg = np.ma.masked_array(summed_eeg, mask=mask)
            print np.max(np.abs(summed_eeg)), summed_eeg.shape
            vmax = 1000
            vmin = -vmax
            clr = lambda phi: plt.cm.PRGn((phi - vmin) / (vmax - vmin))

            clrs = clr(summed_eeg)

            for time_idx in range(0, num_tsteps, 5):

                plt.close("all")
                fig = plt.figure(figsize=[9, 9])

                ax = fig.add_subplot(111, projection='3d', title="EEG at t = {0:.01f} ms".format(tvec[time_idx]))
                ax.set_aspect('equal')
                ax.axis('off')
                ax.set_xlim3d(-65000, 65000)
                ax.set_ylim3d(-65000, 65000)
                ax.set_zlim3d(-65000, 65000)
                ax.view_init(10, 0)

                # colorbar
                cax = fig.add_axes([0.25, 0.1, 0.5, 0.01])
                m = plt.cm.ScalarMappable(cmap=plt.cm.PRGn)
                ticks = np.linspace(vmin, vmax, 5) # global normalization
                m.set_array(ticks)
                cbar = fig.colorbar(m, cax=cax, #format='%3.6f',
                                    extend='both', orientation='horizontal')
                cbar.outline.set_visible(False)
                cbar.set_ticks(ticks)
                #unit is pV
                # cax.set_xticklabels([format(t, '3.1f') for t in ticks])
                cbar.set_label(r'$\phi$ (pV)', labelpad=1.)

                surf = ax.plot_surface(eeg_coords[:, 0].reshape(num_theta, num_phi),
                                       eeg_coords[:, 1].reshape(num_theta, num_phi),
                                       eeg_coords[:, 2].reshape(num_theta, num_phi),
                                       rstride=1, cstride=1, facecolors=clrs[:, :, time_idx, :],
                                       linewidth=0, antialiased=False)

                    # surf.set_facecolors(clrs[:, :, time_idx])
                    # ax.draw()
                fig.savefig(join("..", "eeg_anim",
                                 "eeg_sphere_%s_%04d.png" % (fig_name, time_idx)), dpi=200)


def Population(input_types, celltypes, conductance_type, num_cells):
    """ Run with
        mpirun -np 4 python example_mpi.py
    """
    from mpi4py import MPI

    class Tags:
        def __init__(self):
            self.READY = 0
            self.DONE = 1
            self.EXIT = 2
            self.START = 3
            self.ERROR = 4
    tags = Tags()
    # Initializations and preliminaries
    comm = MPI.COMM_WORLD   # get MPI communicator object
    size = comm.size        # total number of processes
    rank = comm.rank        # rank of this process
    status = MPI.Status()   # get MPI status object
    num_workers = size - 1

    if size == 1:
        print "Can't do MPI with one core!"
        sys.exit()

    if rank == 0:

        print("\033[95m Master starting with %d workers\033[0m" % num_workers)
        task = 0
        # num_cells = 400

        # celltypes = ['L23', "L5"]
        # input_types = ["changing_pathways"]#"tuft_pulse", "basal_pulse"]

        num_tasks = len(celltypes) * len(input_types) * num_cells

        for cell_name in celltypes:
            for input_type in input_types:
                for cell_idx in range(0, num_cells):
                    task += 1
                    sent = False
                    while not sent:
                        data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
                        source = status.Get_source()
                        tag = status.Get_tag()
                        if tag == tags.READY:
                            comm.send([cell_name, input_type, cell_idx], dest=source, tag=tags.START)
                            print "\033[95m Sending task %d/%d to worker %d\033[0m" % (task, num_tasks, source)
                            sent = True
                        elif tag == tags.DONE:
                            print "\033[95m Worker %d completed task %d/%d\033[0m" % (source, task, num_tasks)
                        elif tag == tags.ERROR:
                            print "\033[91mMaster detected ERROR at node %d. Aborting...\033[0m" % source
                            for worker in range(1, num_workers + 1):
                                comm.send([None, None, None], dest=worker, tag=tags.EXIT)
                            sys.exit()
                success = sum_and_remove(conductance_type, cell_name, input_type, num_cells, True)
                if not success:
                    print "Failed to sum. Exiting"
                    for worker in range(1, num_workers + 1):
                        comm.send([None, None, None, None, None, None], dest=worker, tag=tags.EXIT)
        for worker in range(1, num_workers + 1):
            comm.send([None, None, None], dest=worker, tag=tags.EXIT)
        print("\033[95m Master finishing\033[0m")
    else:

        while True:
            comm.send(None, dest=0, tag=tags.READY)
            [cell_name, input_type, cell_idx] = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
            tag = status.Get_tag()
            if tag == tags.START:
                # Do the work here
                print "\033[93m%d put to work on %s %s %s cell %d\033[0m" % (rank, cell_name, input_type, conductance_type, cell_idx)
                # try:
                print "python main.py %s %s %s %d" % (cell_name, input_type, conductance_type, cell_idx)
                os.system("python main.py %s %s %s %d" % (cell_name, input_type, conductance_type, cell_idx))
                # except:
                #     print "\033[91mNode %d exiting with ERROR\033[0m" % rank
                #     comm.send(None, dest=0, tag=tags.ERROR)
                #     sys.exit()
                comm.send(None, dest=0, tag=tags.DONE)
            elif tag == tags.EXIT:
                print "\033[93m%d exiting\033[0m" % rank
                break
        comm.send(None, dest=0, tag=tags.EXIT)


def PopulationSerial(input_types, celltypes, conductance_type, num_cells):

    print("\033[95m Master starting with 0 workers\033[0m" )

    for cell_name in celltypes:
        for input_type in input_types:
            for cell_idx in range(0, num_cells):
                print cell_name, input_type, conductance_type, cell_idx
                os.system("python main.py %s %s %s %d" % (cell_name, input_type, conductance_type, cell_idx))
            success = sum_and_remove(conductance_type, cell_name, input_type, num_cells, True)
            if not success:
                print "Failed to sum. Exiting"



if __name__ == '__main__':

    num_cells = 1000
    # plot_population_VSD("basal_pulse",
    #                     ["BS1173.CNG.swc",  "2007-07-14-A.CNG.swc"],#, "Cell-6-8-06-S3A.CNG.swc"],
    #                     "passive", num_cells)
    # sys.exit()
    input_types = ["changing_pathways"]#"basal_pulse"]#"changing_pathways"]#"tuft_pulse", "basal_pulse"][:]
    celltypes = ['human_L5','human_L23', 'L5', "L23",
                 "hbp_L23_PC_cADpyr229_1", "hbp_L23_PC_cADpyr229_5"][0:1]

    # PopulationSerial(input_types, celltypes, "passive", num_cells)
    # sum_eeg(input_types, celltypes, "passive", num_cells)
    # plot_eeg_sphere(celltypes, input_types, "passive", num_cells)

    # plot_population_eeg("passive", celltypes[0], input_types[0], num_cells)


    plot_combined_population_EEG("passive",
                                 ["human_L5", "human_L23",],
                                 "changing_pathways", num_cells)
    # compare_populations("passive", "changing_pathways", 2000)
